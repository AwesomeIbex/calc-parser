use std::cmp::min;
use std::str::Chars;

use anyhow::{anyhow, Context};
use anyhow::Error;
use std::num::ParseIntError;

enum Bracket {
    OpenBracket,
    CloseBracket,
}

impl Bracket {
    fn get_bracket_char(&self) -> char {
        match self {
            Bracket::OpenBracket => 'e',
            Bracket::CloseBracket => 'f'
        }
    }
}

enum Operator {
    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE,
}

impl Operator {
    fn determine_operator(char: &char) -> Result<Operator, Error> {
        match char {
            'a' => Ok(Operator::ADD),
            'b' => Ok(Operator::SUBTRACT),
            'c' => Ok(Operator::MULTIPLY),
            'd' => Ok(Operator::DIVIDE),
            _ => Err(anyhow!("Failed to find a matching operator"))
        }
    }
}

fn main() {}

fn parse(param: &str) -> Result<i32, Error> {
    let mut param = param.to_lowercase();
    while contains_bracket(&param) {
        param = resolve_bracket(&param)?
    };

    let first_operator = param.chars()
        .find(|char| char.is_alphabetic()).context("Cannot find an input with any operators")?;
    let numbers = param.split(first_operator).map(|number| number.to_lowercase()).collect::<Vec<String>>();

    let mut previous = numbers.first()
        .context(format!("Failed to find any numbers, param {}", param))
        ?.clone()
        .parse::<i32>()?;
    let mut op = first_operator;
    let mut current = String::new();

    numbers.last()
        .context("Failed to read any additional number set")?
        .chars()
        .for_each(|char| {
            if char.is_numeric() {
                current.push_str(&char.to_string())
            } else {
                let num = chars_to_num(&current).expect(&format!("Failed to parse the numset {}", current));
                let result = handle_operator(previous, num, &op).unwrap();
                previous = result;
                op = char;
                current.clear();
            }
        });

    // Special case for final numset
    let num = chars_to_num(&current)?;
    let result = handle_operator(previous, num, &op)?;
    previous = result;

    Ok(previous)
}

fn contains_bracket(param: &str) -> bool {
    param.contains(Bracket::OpenBracket.get_bracket_char())
}

fn resolve_bracket(param: &str) -> Result<String, Error> {
    let mut param = param.to_lowercase();
    let (first_closing_bracket, last_open_bracket) = find_smallest_bracket_set(&param)?;

    let bracket_slice = param.clone().drain(last_open_bracket..first_closing_bracket).collect::<String>();
    let resolved_bracket = parse(&bracket_slice)?.to_string();

    let (last_open_inclusive, first_closing_inclusive) = get_inclusive_brackets(&param, first_closing_bracket, last_open_bracket);
    param.replace_range(last_open_inclusive..first_closing_inclusive, &resolved_bracket);
    Ok(param)
}

fn find_smallest_bracket_set(param: &String) -> Result<(usize, usize), Error> {
    let first_closing_bracket = param.find(Bracket::CloseBracket.get_bracket_char())
        .context("Failed to read a bracket char")?;
    let last_open_bracket = param.chars()
        .rev()
        .collect::<String>()
        .find(Bracket::OpenBracket.get_bracket_char())
        .context("Failed to read a bracket char")?;
    let last_open_bracket = param.len() - last_open_bracket;
    Ok((first_closing_bracket, last_open_bracket))
}

fn get_inclusive_brackets(param: &String, first_closing_bracket: usize, last_open_bracket: usize) -> (usize, usize) {
    let last_open_inclusive = last_open_bracket - 1;
    let first_closing_inclusive = min(first_closing_bracket + 1, param.len());
    (last_open_inclusive, first_closing_inclusive)
}

fn chars_to_num(current: &String) -> Result<i32, Error> {
    println!("Current {}", current);
    match current.parse::<i32>() {
        Ok(val) => Ok(val),
        Err(e) => Err(anyhow!("Failed to parse {} into an integer {}", current, e))
    }
}

fn handle_operator(last: i32, current: i32, op: &char) -> Result<i32, Error> {
    Ok(match Operator::determine_operator(&op)? {
        Operator::ADD => last + current,
        Operator::SUBTRACT => last - current,
        Operator::MULTIPLY => last * current,
        Operator::DIVIDE => last / current,
    })
}

// Largely this is all black box testing, i'd do a little white box testing with more time
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_20() {
        assert_eq!(parse("3a2c4").unwrap(), 20)
    }

    #[test]
    fn test_parse_17() {
        assert_eq!(parse("32a2d2").unwrap(), 17)
    }

    #[test]
    fn test_parse_14208() {
        assert_eq!(parse("500a10b66c32").unwrap(), 14208)
    }

    #[test]
    fn test_parse_235() {
        assert_eq!(parse("3ae4c66fb32").unwrap(), 235)
    }

    #[test]
    fn test_parse_990() {
        assert_eq!(parse("3c4d2aee2a4c41fc4f").unwrap(), 990)
    }
}